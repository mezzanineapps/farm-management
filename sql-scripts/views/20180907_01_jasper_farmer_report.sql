BEGIN;
DROP VIEW IF EXISTS jasper_farmer_report;
CREATE OR REPLACE VIEW jasper_farmer_report
AS
SELECT farmer._id_,
       farmer.firstname,
       farmer.lastname,
       farmer.mobilenumber,
       farmer.datetime_created
FROM farmer order by farmer.firstname;
COMMIT;
