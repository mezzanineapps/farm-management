#!/usr/bin/env python
import re
from xml.dom import minidom
import fnmatch
import os
outFile = open('lang/en.lang','w')
inFiles = ['lang/add.input','r','lang/com.input']

def removeDuplicates(arr):
    result = [];
    for element in arr:
        alreadyThere = False;
        for resultElement in result:
            if resultElement == element:
                alreadyThere = True;
        if alreadyThere == False:
            result.append(element);
    return result;

def getLabels(xmlFile):
    
    xmldoc = minidom.parse(xmlFile)

    labelList = xmldoc.getElementsByTagName('textfield')
    labelList.extend(xmldoc.getElementsByTagName('submit'))
    labelList.extend(xmldoc.getElementsByTagName('menuitem'))
    labelList.extend(xmldoc.getElementsByTagName('view'))
    labelList.extend(xmldoc.getElementsByTagName('rowAction'))
    labelList.extend(xmldoc.getElementsByTagName('button'))
    labelList.extend(xmldoc.getElementsByTagName('checkbox'))
    labelList.extend(xmldoc.getElementsByTagName('select'))
    labelList.extend(xmldoc.getElementsByTagName('invite'))
    labelList.extend(xmldoc.getElementsByTagName('datefield'))
    labelList.extend(xmldoc.getElementsByTagName('info'))
    labelList.extend(xmldoc.getElementsByTagName('fileupload'))
    labelList.extend(xmldoc.getElementsByTagName('action'))
    labelList.extend(xmldoc.getElementsByTagName('textarea'))
    labelList.extend(xmldoc.getElementsByTagName('gpsfield'))

    titleList = xmldoc.getElementsByTagName('table')

    headingList = xmldoc.getElementsByTagName('column')

    elements = []

    for item in labelList:
            elements.append(item.attributes['label'].value)

    for item in titleList:
        try:
            elements.append(item.attributes['title'].value)
        except:
            continue

    for item in headingList:
            elements.append(item.attributes['heading'].value)

    return elements

#Print to file
toSort = []
#(writes any outside vxml lang entries to the file first)
for inFile in inFiles:
    try: 
        inFile = open(inFile, 'r')
    except:
        continue
    for line in inFile:
        toSort.append(line);
        
matches = []
for root, dirnames, filenames in os.walk('./'):
 
  for filename in fnmatch.filter(filenames, '*.vxml'):
      if 'communicator_views' in os.path.join(root, filename):
          continue
      matches.append(os.path.join(root, filename))


for match in matches:
    #print(match)
    labels = getLabels(match)
    for label in labels:
        entry = label.split('.')[-1].replace('_',' ')
        outLabel = label + '=' + entry[0].capitalize() + entry[1:]
        toSort.append(outLabel + '\n');


sortedProperties = sorted(toSort);
filteredProperties = removeDuplicates(sortedProperties);

prev = '';
for line in filteredProperties:
    splitString = line.split('.');
    if prev == '':
        prev = splitString[0];
    if prev != splitString[0]:
        line = '\n' + line;

    prev = splitString[0];

    #if splitString[0] == 'info':
    #    line = line + ':';


    #final horrible hack: add units to weight, land size and monetary values
    #if line == 'info.Weight=Weight\n':
    #    line = 'info.Weight=Weight (kg)\n';
    #elif line == 'input.Weight=Weight\n':
    #    line = 'input.Weight=Weight (kg)\n';
    #elif line == 'column_heading.Weight=Weight\n':
    #    line = 'column_heading.Weight=Weight (kg)\n';
    #elif line == 'input.Yield_estimate=Yield estimate\n':
    #    line = 'input.Yield_estimate=Yield estimate (kg/Acre)\n';
    #elif line == 'column_heading.Yield_estimate=Yield estimate\n':
    #    line = 'column_heading.Yield_estimate=Yield estimate (kg/Acre)\n';

    outFile.write(line);

outFile.flush()

        
